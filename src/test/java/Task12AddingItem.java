import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

public class Task12AddingItem {

    private WebDriver driver;
    private WebDriverWait wait;

    private final String adminPage = "http://localhost/litecart/admin";

    private final String login = "//input[@name='username']";
    private final String password = "//input[@name='password']";
    private final String submitLogin = "//button[@name='login']";

    private final String catalog = "//a[contains(@href, 'catalog')]";
    private final String addProduct = "//a[contains(@href, 'edit_product')]";

    // General tab
    private String itemName = null;
    private int codeValue = 0;

    private final String enabled = "//*[contains(text(), 'Enabled')]";
    private final String name = "//input[@name='name[en]']";
    private final String code = "//input[@name='code']";
    private final String unisex = "//*[contains(text(), 'Unisex')]/parent::*//*[@type='checkbox']";
    private final String quantity = "//input[@name='quantity']";
    private final String fileUpload = "//input[@name='new_images[]']";
    private final String validFrom = "//input[@name='date_valid_from']";
    private final String validTo = "//input[@name='date_valid_to']";

    // Information tab
    private final String informationTab = "//a[contains(@href, 'tab-information')]";
    private final String manufacturer = "//select[@name='manufacturer_id']";
    private final String keywords = "//input[@name='keywords']";
    private final String shortDescription = "//input[@name='short_description[en]']";
    private final String fullDescription = "//div[@class='trumbowyg-editor']";
    private final String headTitle = "//input[@name='head_title[en]']";
    private final String metaDescription = "//input[@name='meta_description[en]']";

    // Prices tab
    private final String pricesTab = "//a[contains(@href, 'tab-prices')]";
    private final String purchasePriceValue = "//input[@name='purchase_price']";
    private final String purchasePriceCurrency = "//select[@name='purchase_price_currency_code']";
    private final String priceInclTaxUsd = "//input[@name='gross_prices[USD]']";
    private final String priceInclTaxEur = "//input[@name='gross_prices[EUR]']";

    private final String saveBtn = "//button[@name='save']";

    // Catalog
    private final String headerItems = "//form[@name='catalog_form']//th";
    private final String nameRows = "//form[@name='catalog_form']//tr[contains(@class, 'row')]" ;


    private final SimpleDateFormat formatter = new SimpleDateFormat("MMddyyyy");
    private String nameColumn = "Name";
    private final String innerText = "innerText";

    @Before
    public void start() {
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 10);
    }

    @Test
    public void addItem() {
        driver.get(adminPage);

        // Login screen
        String adminCreds = "admin";
        enterText(adminCreds, login);
        enterText(adminCreds, password);
        click(submitLogin);

        sleep(1000);

        // Open Add product page
        click(catalog);
        click(addProduct);

        sleep(1000);
        // General tab
        click(enabled);
        enterText(createRandomName(), name);
        enterText(String.valueOf(codeValue), code);
        click(unisex);
        enterText("11", quantity);

        WebElement uploadFile = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(fileUpload)));
        File file = new File("src/test/resources/roboduck.png");
        String absolutePath = file.getAbsolutePath();

        uploadFile.sendKeys(absolutePath);

        enterText(getCurrentDate(), validFrom);
        enterText(getDateInAMonth(), validTo);

        // Information tab
        click(informationTab);
        sleep(1000);

        selectByVisibleText("ACME Corp.", manufacturer);

        enterText("serious killer robo duck test", keywords);
        enterText("Rubber duck police test", shortDescription);
        enterText("Lorem ipsum pomidorum rubber duck police full description test", fullDescription);
        enterText("Roboduck test", headTitle);
        enterText("Meta description of roboduck test", metaDescription);

        // Prices tab
        click(pricesTab);
        sleep(2000);

        enterText("18", purchasePriceValue);

        selectByVisibleText("US Dollars", purchasePriceCurrency);

        enterText("124", priceInclTaxUsd);
        enterText("113", priceInclTaxEur);

        click(saveBtn);

        List<WebElement> headers = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(this.headerItems)));
        int itemNameIndex = findColumnIndex(headers, this.nameColumn);

        if (itemNameIndex < 0) {
            String message = String.format("The item column named \"%s\" not found.", this.nameColumn);
            Assert.assertTrue(message, false);
        }

        List<WebElement> rows = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(this.nameRows)));
        int numberOfRows = rows.size();

        String message = String.format("The item named \"%s\" not listed in the catalog.", this.itemName);
        Assert.assertTrue(message, isItemListed(itemNameIndex, numberOfRows));

        sleep(3000);
    }

    private boolean isItemListed(int itemNameIndex, int numberOfRows) {
        if (numberOfRows < 1) {
            return false;
        }

        for (int i = 1; i <= numberOfRows; i++) {
            WebElement itemName = driver.findElement(By.xpath(String.format("%s[%s]/td[%s]", this.nameRows, i, itemNameIndex)));
            String name = itemName.getAttribute(innerText).trim();
            if (name.equals(this.itemName)) {
                System.out.println("Item named " + this.itemName + " is listed in the catalog.");
                return true;
            }
        }
        return false;
    }

    private void sleep(int i) {
        try {
            Thread.sleep(i);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private String getCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        return formatter.format(calendar.getTime());
    }

    private String getDateInAMonth() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, 1);
        return formatter.format(calendar.getTime());
    }

    private void enterText(String text, String locator) {
        WebElement field = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
        field.clear();
        field.sendKeys(text);
        System.out.println("Entering text \"" + text + "\" to the field " + locator);
    }

    private void click(String locator) {
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)))
                .click();
    }

    private void selectByVisibleText(String text, String locator) {
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
        Select select = new Select(element);
        select.selectByVisibleText(text);
    }

    private String createRandomName() {
        Random random = new Random();
        codeValue = random.nextInt(999999999);
        itemName = "autoDuck" + codeValue;
        return itemName;
    }

    private int findColumnIndex(List<WebElement> columns, String columnName) {
        int numberOfColumns = columns.size();

        for (int i = 0; i < numberOfColumns; ++i) {
            String actualName = columns.get(i).getAttribute(innerText);
            if (actualName.equals(columnName)) {
                return i + 1;
            }
        }
        return -1;
    }

    /*@After
    public void stop() {
        driver.quit();
        driver = null;
    }*/

}
