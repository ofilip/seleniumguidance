import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class Task7LeftMenu {


    private final String menuElements = "//li[@id='app-']";
    private final String nestedMenuElements = "//li[@class='selected']//li";
    private final String chartsOnInitialPage = "//div[@id='chart-sales-monthly']";

    private final String header = "//h1";

    private WebDriver driver;
    private WebDriverWait wait;

    @Before
    public void start() {
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 10);
    }

    @Test
    public void clickThroughMenuCheckHeaders() {
        driver.get("http://localhost/litecart/admin");

        driver.findElement(By.name("username")).sendKeys("admin");
        driver.findElement(By.name("password")).sendKeys("admin");

        driver.findElement(By.name("login")).click();

        WebDriverWait wait = new WebDriverWait(driver, 7);

        // make sure page is loaded
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(chartsOnInitialPage)));

        // find and count first level menu elements
        List<WebElement> elements = driver.findElements(By.xpath(menuElements));
        int primaryElementsCount = elements.size();

        for (int i = 0; i < primaryElementsCount; i++) {
            wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(menuElements)));
            elements = driver.findElements(By.xpath(menuElements)); // update first level elements' links
            WebElement current = elements.get(i);
            wait.until(ExpectedConditions.elementToBeClickable(current));
            current.click();

            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(header)));

            // find and count nested elements
            List<WebElement> nestedElements = driver.findElements(By.xpath(nestedMenuElements));
            int nestedElementsCount = nestedElements.size();

            for (int j = 0; j < nestedElementsCount; j++) {
                wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(nestedMenuElements)));
                nestedElements = driver.findElements(By.xpath(nestedMenuElements)); // update nested level elements' links
                WebElement currentNested = nestedElements.get(j);
                wait.until(ExpectedConditions.elementToBeClickable(currentNested));
                currentNested.click();

                wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(header)));
            }
        }
    }

    @After
    public void stop() {
        driver.quit();
        driver = null;
    }
}


