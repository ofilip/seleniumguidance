import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task10VerifyItemPage {
    //locators
    private final String mainPage = "http://localhost/litecart/";
    private final String firstItemAtCampaigns = "//div[contains(@id, 'campaign')]//li[contains(@class, 'product')][1]";
    private final String name = ".//*[@class='name']";
    private final String price = ".//*[@class='regular-price']";
    private final String campaignPrice = ".//*[@class='campaign-price']";

    private final String itemPageTitle = "//div[contains(@id, 'product')]//h1[@class='title']";
    private final String itemPagePrice = "//div[contains(@id, 'product')]//*[@class='regular-price']";
    private final String itemPageCampaignPrice = "//div[contains(@id, 'product')]//*[@class='campaign-price']";

    // attributes
    private final String innerText = "innerText";
    private final String color = "color";

    // tags
    private final String strikeThrough = "s";
    private final String strong = "strong";
    private final String fontSize = "font-size";

    private WebDriver driver;
    private WebDriverWait wait;

    private final String browser = "ie"; //"ie" "firefox" "chrome"

    @Before
    public void start() {
        if (this.browser.equals("ie")) {
            InternetExplorerOptions capabilities = new InternetExplorerOptions();
            capabilities.ignoreZoomSettings();
            driver = new InternetExplorerDriver(capabilities);
            wait = new WebDriverWait(driver, 10);
        }
        if (this.browser.equals("firefox")) {
            driver = new FirefoxDriver();
            wait = new WebDriverWait(driver, 10);
        }
        if (this.browser.equals("chrome")) {
            driver = new ChromeDriver();
            wait = new WebDriverWait(driver, 10);
        }
    }

    @Test
    public void verifyItemPageCorrespondsToItem() throws IllegalAccessException {
        driver.get(mainPage);

        // find the item and save name and prices
        WebElement item = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(firstItemAtCampaigns)));
        String itemName = getAttribute(item, this.name, this.innerText);
        String price = getAttribute(item, this.price, this.innerText);
        String campaignPrice = getAttribute(item, this.campaignPrice, this.innerText);

        // check price styles on the main page
        String priceStyle = getTagName(item, this.price);
        System.out.println("priceStyle: " + priceStyle);
        Assert.assertEquals("Main page. Price text style at the is not line-through.", this.strikeThrough, priceStyle);

        String priceColor = getCssValue(item, this.price, this.color);
        RGB rgb = new RGB(priceColor);
        System.out.println("priceColor: " + rgb);
        Assert.assertTrue("Main page. Price color is not grey.", (rgb.getR() == rgb.getG()) && (rgb.getR() == rgb.getB()));

        String campaignPriceStyle = getTagName(item, this.campaignPrice);
        System.out.println("campaignPriceStyle: " + campaignPriceStyle);
        Assert.assertEquals("Main page. Campaign Price text style is not bold", this.strong, campaignPriceStyle);

        String campaignPriceColor = getCssValue(item, this.campaignPrice, this.color);
        rgb = new RGB(campaignPriceColor);
        System.out.println("campaignPriceColor: " + rgb);
        Assert.assertTrue("Main page. Campaign Price color is not Red.", (rgb.getG() == 0) && (rgb.getB() == 0));

        double priceSize = Double.parseDouble(
                getCssValue(item, this.price, this.fontSize)
                        .replaceAll("px", ""));
        System.out.println("priceSize: " + priceSize);
        double campaignPriceSize = Double.parseDouble(
                getCssValue(item, this.campaignPrice, this.fontSize)
                        .replaceAll("px", ""));
        System.out.println("campaignPriceSize: " + campaignPriceSize);
        Assert.assertTrue("Main page. Campaign price is not bigger than regular price.", campaignPriceSize > priceSize);

        // open item page and save name and prices
        wait.until(ExpectedConditions.elementToBeClickable(item)).click();
        String itemPageTitle = getAttribute(this.itemPageTitle, this.innerText);
        String itemPagePrice = getAttribute(this.itemPagePrice, this.innerText);
        String itemPageCampaignPrice = getAttribute(this.itemPageCampaignPrice, this.innerText);

        // check price styles on the main page
        String itemPagePriceStyle = getTagName(this.itemPagePrice);
        System.out.println("itemPagePriceStyle: " + itemPagePriceStyle);
        Assert.assertEquals("Item page. Price text style is not strike-through.", this.strikeThrough, itemPagePriceStyle);

        String itemPagePriceColor = getCssValue(this.itemPagePrice, this.color);
        rgb = new RGB(itemPagePriceColor);
        Assert.assertTrue("Item page. Price color at the is not grey.", (rgb.getR() == rgb.getG()) && (rgb.getR() == rgb.getB()));

        String itemPageCampaignPriceStyle = getTagName(this.itemPageCampaignPrice);
        System.out.println("itemPageCampaignPriceStyle: " + itemPageCampaignPriceStyle);
        Assert.assertEquals("Item page. Campaign Price text is not bold.", this.strong, itemPageCampaignPriceStyle);

        String itemPageCampaignPriceColor = getCssValue(this.itemPageCampaignPrice, this.color);
        rgb = new RGB(itemPageCampaignPriceColor);
        System.out.println("itemPageCampaignPriceColor: " + rgb);
        Assert.assertTrue("Item page. Campaign Price color is not red.", (rgb.getG() == 0) && (rgb.getB() == 0));

        double itemPagePriceSize = Double.parseDouble(
                getCssValue(this.itemPagePrice, this.fontSize)
                        .replaceAll("px", ""));
        System.out.println("itemPagePriceSize: " + itemPagePriceSize);
        double itemPageCampaignPriceSize = Double.parseDouble(
                getCssValue(this.itemPageCampaignPrice, this.fontSize)
                        .replaceAll("px", ""));
        System.out.println("itemPageCampaignPriceSize: " + itemPageCampaignPriceSize);
        Assert.assertTrue("Item page. Campaign price is not bigger than regular price.", itemPageCampaignPriceSize > itemPagePriceSize);

        // compare name and price values
        String message2 = "%s at the Main page and at the Item page doesn't coincide";
        Assert.assertEquals(String.format(message2, "Item name"), itemPageTitle, itemName);
        Assert.assertEquals(String.format(message2, "Price"), itemPagePrice, price);
        Assert.assertEquals(String.format(message2, "Campaign Price"), itemPageCampaignPrice, campaignPrice);
    }

    private String getTagName(WebElement element, String childLocator) {
        return wait.until(ExpectedConditions
                .visibilityOf(element.findElement(By.xpath(childLocator))))
                .getTagName();
    }

    private String getTagName(String locator) {
        return wait.until(ExpectedConditions
                .visibilityOfElementLocated(By.xpath(locator)))
                .getTagName();
    }

    private String getAttribute(WebElement element, String locator, String attribute) {
        return wait.until(ExpectedConditions
                .visibilityOf(element.findElement(By.xpath(locator))))
                .getAttribute(attribute);
    }

    private String getAttribute(String locator, String attribute) {
        return wait.until(ExpectedConditions
                .visibilityOfElementLocated(By.xpath(locator)))
                .getAttribute(this.innerText);
    }

    private String getCssValue(WebElement element, String locator, String value) {
        return wait.until(ExpectedConditions
                .visibilityOf(element.findElement(By.xpath(locator))))
                .getCssValue(value);
    }

    private String getCssValue(String locator, String value) {
        return wait.until(ExpectedConditions
                .visibilityOfElementLocated(By.xpath(locator)))
                .getCssValue(value);
    }

    @After
    public void stop() {
        driver.quit();
        driver = null;
    }
}

class RGB {
    private int r;
    private int g;
    private int b;

    RGB(String input) throws IllegalAccessException {
        Pattern rgbaPattern = Pattern.compile("rgba *\\( *([0-9]+), *([0-9]+), *([0-9]+), *([0-9]+) *\\)");
        Matcher rgbaMatcher = rgbaPattern.matcher(input);

        Pattern rgbPattern = Pattern.compile("rgb *\\( *([0-9]+), *([0-9]+), *([0-9]+) *\\)");
        Matcher rgbMatcher = rgbPattern.matcher(input);

        if (rgbMatcher.matches()) {
            this.r = Integer.valueOf(rgbMatcher.group(1));
            this.g = Integer.valueOf(rgbMatcher.group(2));
            this.b = Integer.valueOf(rgbMatcher.group(3));
        } else if (rgbaMatcher.matches()) {
            this.r = Integer.valueOf(rgbaMatcher.group(1));
            this.g = Integer.valueOf(rgbaMatcher.group(2));
            this.b = Integer.valueOf(rgbaMatcher.group(3));
        } else {
            throw new IllegalAccessException("String \"" + input + "\" doesn't match the RGB/RGBA pattern.");
        }
    }

    public int getR() {
        return r;
    }

    public int getG() {
        return g;
    }

    public int getB() {
        return b;
    }

    @Override
    public String toString() {
        return "RGB{" +
                "r=" + r +
                ", g=" + g +
                ", b=" + b +
                '}';
    }
}
