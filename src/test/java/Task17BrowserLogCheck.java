import com.google.common.io.Files;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.events.AbstractWebDriverEventListener;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class Task17BrowserLogCheck {
    private EventFiringWebDriver driver;
    private WebDriverWait wait;

    private final String adminUrl = "http://localhost/litecart/admin/";

    private final String adminLinks = "//a[contains(@href, 'admin')]";
    private final String tableData = "//td";
    private final String foldersNotOpen = "//*[contains(@class, 'folder') and not(contains(@class, 'open'))]/following-sibling::a";
    private final String items = "(//td//img/following-sibling::a)";
    private final String itemPageHeader = "//h1[contains(text(), 'Edit Product')]";

    private static class MyListener extends AbstractWebDriverEventListener {
        @Override
        public void afterChangeValueOf(WebElement element, WebDriver driver, CharSequence[] keysToSend) {
            System.out.println(element + " value changed to: " + keysToSend);
        }

        @Override
        public void onException(Throwable throwable, WebDriver driver) {
            System.out.println("On Exception: " + throwable);
            File tmp = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            File screen = new File("screen-" + System.currentTimeMillis() + ".png");
            try {
                Files.copy(tmp, screen);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("Screenshot name: " + screen);
        }
    }

    @Before
    public void start() {
        driver = new EventFiringWebDriver(new ChromeDriver());
        driver.register(new MyListener());
        wait = new WebDriverWait(driver, 10);
    }

    @Test
    public void Task17BrowserLogCheck() { // Check that no entries appeared in the browser log
        loginAsAdmin();
        navigateToCatalogItemsTable();

        List<WebElement> foldersToOpen = null;
        int numberOfFoldersToOpen = 0;
        try {
            foldersToOpen = driver.findElements(By.xpath(foldersNotOpen));
            numberOfFoldersToOpen = foldersToOpen.size();
        } catch (Exception ignore) {
        }
        while (numberOfFoldersToOpen > 0) {
            for (int i = 1; i <= numberOfFoldersToOpen; i++) {
                try {
                    wait.until(ExpectedConditions.elementToBeClickable(By.xpath(foldersNotOpen + "[" + i + "]"))).click();
                } catch (Exception ignore) {
                }
            }
            try {
                foldersToOpen = driver.findElements(By.xpath(foldersNotOpen));
                numberOfFoldersToOpen = foldersToOpen.size();
            } catch (Exception e) {
                numberOfFoldersToOpen = 0;
            }
        }

        List<WebElement> itemsList = null;
        int numberOfItems = 0;
        try {
            itemsList = driver.findElements(By.xpath(items));
            numberOfItems = itemsList.size();
        } catch (Exception ignore) {
        }

            for (int i = 1; i <= numberOfItems; i++) {
                try {
                    wait.until(ExpectedConditions.elementToBeClickable(By.xpath(items + "[" + i + "]"))).click();
                } catch (Exception ignore) {
                }

                wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(itemPageHeader)));
                driver.manage().logs().get("browser").getAll().forEach(l -> System.out.println(l));
                driver.navigate().back();
            }

    }

    private void navigateToCatalogItemsTable() {
        driver.get("http://localhost/litecart/admin/?app=catalog&doc=catalog");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(tableData)));
    }

    public void loginAsAdmin() {
        driver.get(adminUrl);
        wait.until(ExpectedConditions.elementToBeClickable(By.name("username")))
                .sendKeys("admin");
        wait.until(ExpectedConditions.elementToBeClickable(By.name("password")))
                .sendKeys("admin");
        wait.until(ExpectedConditions.elementToBeClickable(By.name("login"))).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(this.adminLinks)));
    }

    @After
    public void stop() {
        driver.quit();
        driver = null;
    }
}
