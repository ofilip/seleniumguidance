import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Task9_1CountriesSorting {
    private final String countriesUrl = "http://localhost/litecart/admin/?app=countries&doc=countries";

    private final String countriesHeaderItems = "//table[@class='dataTable']//tr[@class='header']/th";
    private final String countriesRows = "//table[@class='dataTable']//tr[@class='row']";
    private final String zonesHeaderItems = "//table[@id='table-zones']//tr[@class='header']/th";
    private final String zonesRows = "//table[@id='table-zones']//tr[not(contains(@class, 'header'))]";

    private final String countryColumnName = "Name";
    private final String zonesColumnName = "Zones";
    private final String innerText = "innerText";
    private final String zoneNameColumn = "Name";

    private WebDriver driver;
    private WebDriverWait wait;

    @Before
    public void start() {
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 10);

        driver.get(countriesUrl);
        driver.findElement(By.name("username")).sendKeys("admin");
        driver.findElement(By.name("password")).sendKeys("admin");
        driver.findElement(By.name("login")).click();
    }

    @Test
    public void checkCountiesSorting9_1A() { // 30s
        List<WebElement> headers = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(this.countriesHeaderItems)));
        int countryNameIndex = findColumnIndex(headers, this.countryColumnName);

        if (countryNameIndex < 0) {
            String message = String.format("The country column named \"%s\" not found.", this.countryColumnName);
            Assert.assertTrue(message, false);
        }

        List<WebElement> rows = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(this.countriesRows)));
        int numberOfRows = rows.size();

        List<String> countries = new ArrayList<>();
        for (int i = 1; i < numberOfRows; i++) {
            WebElement country = driver.findElement(By.xpath(String.format("%s[%s]/td[%s]", this.countriesRows, i, countryNameIndex)));
            String countryName = country.getAttribute(innerText);
            countries.add(countryName);
        }

        List<String> countriesSortedAlphabetically = new ArrayList<>(countries);
        Collections.sort(countriesSortedAlphabetically);

        Assert.assertEquals("Countries not sorted alphabetically.", countriesSortedAlphabetically, countries);
    }

    @Test
    public void checkZonesSorting9_1B() { //39 s
        List<WebElement> headers = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(this.countriesHeaderItems)));

        int countryNameIndex = findColumnIndex(headers, this.countryColumnName);

        if (countryNameIndex < 0) {
            String message = String.format("The country column named \"%s\" not found.", this.countryColumnName);
            Assert.assertTrue(message, false);
        }

        int timeZoneIndex = findColumnIndex(headers, this.zonesColumnName);
        if (timeZoneIndex < 0) {
            String message = String.format("Zones column column named \"%s\" not found.", this.zonesColumnName);
            Assert.assertTrue(message, false);
        }

        List<WebElement> rows = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(this.countriesRows)));
        int numberOfRows = rows.size();

        for (int i = 1; i < numberOfRows; i++) {
            List<String> zones = new ArrayList<>();
            WebElement timeZone = driver.findElement(By.xpath(String.format("%s[%s]/td[%s]", this.countriesRows, i, timeZoneIndex)));
            int numberOfTimeZones = Integer.parseInt(timeZone.getAttribute(innerText));

            if (numberOfTimeZones > 0) {
                WebElement country = driver.findElement(By.xpath(String.format("%s[%s]/td[%s]/a", this.countriesRows, i, countryNameIndex)));
                country.click();

                List<WebElement> zoneColumns = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(this.zonesHeaderItems)));
                int zoneNameIndex = findColumnIndex(zoneColumns, this.zoneNameColumn);
                if (zoneNameIndex < 0) {
                    String message = String.format("Zone name column named %s not found.", this.zoneNameColumn);
                    Assert.assertTrue(message, false);
                }

                List<WebElement> zoneRows = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(this.zonesRows)));
                int timeZoneNumberOfRows = zoneRows.size();

                for (int j = 1; j < timeZoneNumberOfRows; j++) {
                    WebElement zone = driver.findElement(By.xpath(String.format("%s[%s]/td[%s]", this.zonesRows, j, zoneNameIndex)));
                    String zoneName = zone.getAttribute(innerText);
                    zones.add(zoneName);
                }

                List<String> zonesSortedAlphabetically = new ArrayList<>(zones);
                Collections.sort(zonesSortedAlphabetically);

                Assert.assertEquals("Time zones not sorted alphabetically.", zonesSortedAlphabetically, zones);
                driver.navigate().back();
            }
        }
    }

    /**
     * Goes through the table columns at the given row, finding the index of the column with the specified text
     * @param columns list of columns in the table
     * @param columnName the name of the column
     * @return the index of the column named columnName. -1 when no match found.
     */
    private int findColumnIndex(List<WebElement> columns, String columnName) {
        int numberOfColumns = columns.size();

        for (int i = 0; i < numberOfColumns; ++i) {
            String actualName = columns.get(i).getAttribute(innerText);
            if (actualName.equals(columnName)) {
                return i + 1;
            }
        }
        return -1;
    }

    @After
    public void stop() {
        driver.quit();
        driver = null;
    }

}
