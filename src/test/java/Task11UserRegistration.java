import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Random;

public class Task11UserRegistration {
    private final String entryPageUrl = "http://localhost/litecart/";

    private final String newCustomers = "//*[contains(text(), 'New customer')]";

    private final String firstName = "//input[@name='firstname']";
    private final String lastName = "//input[@name='lastname']";
    private final String address1 = "//input[@name='address1']";
    private final String postCode = "//input[@name='postcode']";
    private final String city = "//input[@name='city']";

    private final String country = "//span[@class='select2-selection select2-selection--single']";

    private final String emailField = "//input[@name='email']";
    private final String phone = "//input[@name='phone']";
    private final String passwordField = "//input[@name='password']";
    private final String confirmPassword = "//input[@name='confirmed_password']";
    private final String createAccount = "//button[@name='create_account']";

    private final String logout = "//aside[@id='navigation']//a[contains(@href, 'logout')]";

    // login form
    private final String login = "//input[@name='email']";
    private final String pass = "//input[@name='password']";
    private String loginButton = "//button[@name='login']";

    private WebDriver driver;
    private WebDriverWait wait;

    private final String countryName = "United States";
    private String emailAddress = null;
    private String password = "password123";

    @Before
    public void setUp() {
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 10);
    }

    @Test
    public void checkUserRegistration() {
        driver.get(entryPageUrl);

        // open new customer window
        click(newCustomers);
        waitForPageLoaded();

        // register new customer
        enterText("TestName", firstName);
        enterText("LastName", lastName);
        enterText("234A Address Str., 98", address1);
        enterText("03123", postCode);
        enterText("Kyiv", city);
        selectCountry(countryName, country);
        enterText(createRandomEmail(), emailField);
        enterText("+381234567890", phone);
        enterText(password, passwordField);
        enterText(password, confirmPassword);
        click(createAccount);

        // exit and login again
        click(logout);

        enterText(emailAddress, login);
        enterText(password, pass);
        click(loginButton);

        click(logout);
    }

    private String createRandomEmail() {
        Random random = new Random();
        int nameEnding = random.nextInt(999999999);
        emailAddress = "testEmail" + nameEnding + "@example.com";
        return emailAddress;
    }

    private void selectCountry(String countryName, String locator) {
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
        element.click();

        Actions actions = new Actions(driver);
        actions.sendKeys(countryName).sendKeys(Keys.ENTER).perform();
    }

    private void enterText(String text, String locator) {
        WebElement field = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
        field.clear();
        field.sendKeys(text);
    }

    private void click(String locator) {
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)))
                .click();
    }

    private String generateLogin() {
        return "test" + Math.random();
    }

    @After
    public void tearDown() {
        driver.quit();
        driver = null;
    }

    public void waitForPageLoaded() {
        ExpectedCondition<Boolean> expectation = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete");
                    }
                };
        try {
            Thread.sleep(1000);
            WebDriverWait wait = new WebDriverWait(driver, 30);
            wait.until(expectation);
        } catch (Throwable error) {
            Assert.fail("Timeout waiting for Page Load Request to complete.");
        }
    }

}
