import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Task13CartInteraction {
    private WebDriver driver;
    private WebDriverWait wait;

    private final String baseUrl = "http://localhost/litecart/";

    private final String firstItem = "(//li[contains(@class, 'product')])[1]";
    private final String addToCart = "//button[@name='add_cart_product']";
    private final String counter = "//div[contains(@id, 'cart')]//span[@class='quantity']";
    private final String dropDown = "//select";
    private final String checkout = "//a[contains(text(), 'Checkout')]";
    private final String confirm = "//button[contains(@name, 'confirm')]";

    private final String summaryRows = "//*[contains(@id, 'checkout-summary-wrapper')]//tr";
    private final String skuCurrentProduct = "//li[@class='item']//span[contains(text(), 'SKU')]";
    private final String remove = "(//button[@name='remove_cart_item'])[1]";
    private final String shortcuts = "//ul[@class='shortcuts']//a";
    private final String itemsPaneAt0Px = "//ul[@class='items' and contains(@style, '0px')]";
    private final String cartEmpty = "//div[@id='checkout-cart-wrapper']//em[contains(text(), 'no items') or contains(text(), 'empty')]";

    private final String innerText = "innerText";

    @Before
    public void start() {
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 10);
    }

    @Test
    public void cartInteractionScenario() {
        openMainPage(baseUrl);

        int numberOfItems = 5;
        for (int i = 0; i < numberOfItems; i++) {
            addFirstListedItemToCart();

            if (i + 1 < numberOfItems) {
                navigateToMainPage();
            }
        }

        navigateToCheckout();

        // remove all products from the cart one by one
        List<String> listOfProductTypes = getAllSkuListed();
        int numberOfItemTypes = listOfProductTypes.size();

        for (int i = 0; i < numberOfItemTypes; i++) {
            removeFirstListedItem();
        }
    }

    private void removeFirstListedItem() {
        navigateToFirstListedElement();

        String currentSkuValue = getCurrentSkuValue();
        System.out.println("Current SKU page just opened: " + currentSkuValue);

        int numberOfRowsBeforeDeletion = getNumberOfRows();
        List<String> initialSkuList = getAllSkuListed();
        System.out.println("All items before deletion: " + initialSkuList);
        Assert.assertTrue("The table with items doesn't contain SKU of the currently opened item.",
                initialSkuList.contains(currentSkuValue));

        boolean isLastItem = initialSkuList.size() < 2;

        currentSkuValue = getCurrentSkuValue();
        System.out.println("Current SKU pre rem: " + currentSkuValue);

        click(this.remove);
        wait.until(ExpectedConditions.invisibilityOfElementWithText(By.xpath("//*[contains(@id, 'checkout-summary-wrapper')]//td[@class='sku']"), currentSkuValue));

        if (!isLastItem) {
            int numberOfRowsAfterDeletion = getNumberOfRows();
            List<String> allSkuListedAfter = getAllSkuListed();
            System.out.println("All items after deletion: " + allSkuListedAfter);
            Assert.assertEquals("Invalid number of rows were deleted from the table (not 1)", numberOfRowsBeforeDeletion - 1, numberOfRowsAfterDeletion);
            Assert.assertFalse("Wrong item was deleted from the cart.", allSkuListedAfter.contains(currentSkuValue));
        } else {
            System.out.println("Waiting for a \"cart empty\" message...");
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(cartEmpty)));
            System.out.print("ok");
        }
    }

    private List<String> getAllSkuListed() {
        List<WebElement> allItemsListed = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[contains(@id, 'checkout-summary-wrapper')]//td[@class='sku']")));
        List<String> allItemsValues = new ArrayList();
        for (WebElement skuElement : allItemsListed) {
            allItemsValues.add(skuElement.getAttribute("innerText").trim());
            System.out.println("InnerText SKU: " + skuElement.getAttribute("innerText").trim());
        }
        return allItemsValues;
    }

    private int getNumberOfRows() {
        List<WebElement> rows = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(this.summaryRows)));
        return rows.size();
    }

    private void navigateToFirstListedElement() {
        List<WebElement> shortcuts = driver.findElements(By.xpath(this.shortcuts));
        if (shortcuts.size() > 0) {
            System.out.println("Shortcuts: " + shortcuts.toString());
            click(shortcuts.get(0));
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(itemsPaneAt0Px)));
        }
    }

    private void navigateToCheckout() {
        click(checkout);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(this.confirm)));
    }

    private void navigateToMainPage() {
        driver.navigate().back();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(this.counter)));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(this.firstItem)));
    }

    private void addFirstListedItemToCart() {
        openFirstItem();
        String quantity = getNumberOfItemsInCart();

        addItemToCart();

        waitCounterToBeUpdated(quantity);
        String quantityAfterAddingItem = getNumberOfItemsInCart();

        verifyOneItemAdded(quantity, quantityAfterAddingItem);
        System.out.println("Quantity after an item was added " + quantityAfterAddingItem);
    }

    private String getCurrentSkuValue() {
        WebElement sku = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(this.skuCurrentProduct)));
        String skuValue = sku.getAttribute("innerText");
        return skuValue.replace("[SKU: ", "").replace("]", "").trim();
    }

    public void openMainPage(String url) {
        driver.get(url);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(this.counter)));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(this.firstItem)));
    }

    public void openFirstItem() {
        click(firstItem);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(addToCart)));
    }

    private void addItemToCart() {
        selectOptionWhenDropdownPresent();
        click(addToCart);
    }

    private void selectOptionWhenDropdownPresent() {
        try {
            WebElement dropDown = new WebDriverWait(driver, 1)
                    .until(ExpectedConditions.visibilityOfElementLocated(By.xpath(this.dropDown)));
            Select select = new Select(dropDown);
            int numberOfOptions = select.getOptions().size();
            System.out.println("Number of options in the dropdown: " + numberOfOptions);
            Random random = new Random();
            int index = random.nextInt(numberOfOptions - 1) + 1;
            System.out.println("Index of item to be selected in the dropdown: " + index);
            select.selectByIndex(index);
        } catch (Exception ignore) {
        }
    }

    private String getNumberOfItemsInCart() {
        WebElement counter = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(this.counter)));
        return counter.getAttribute(innerText);
    }

    private void waitCounterToBeUpdated(String quantity) {
        wait.until(ExpectedConditions.invisibilityOfElementWithText(By.xpath(this.counter), quantity));
    }

    private void verifyOneItemAdded(String quantity, String quantityAfterAddingItem) {
        Assert.assertEquals("Number of items added to the cart is not equal to 1",
                Integer.parseInt(quantity.trim()) + 1, Integer.parseInt(quantityAfterAddingItem.trim()));
    }

    private boolean click(WebElement element) {
        try {
            wait.until(ExpectedConditions.elementToBeClickable(element))
                    .click();
            System.out.println("Click on the element: " + element.toString());
            return true;
        } catch (Exception ignored) {
            System.out.println("Couldn't click on the element: " + element.toString());
        } finally {
            return false;
        }
    }

    private boolean click(String locator) {
        try {
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)))
                    .click();
            System.out.println("Click on the element: " + locator);
            return true;
        } catch (Exception ignored) {
            System.out.println("Couldn't click on the element: " + locator);
        } finally {
            return false;
        }
    }

    @After
    public void stop() {
        driver.quit();
        driver = null;
    }
}