import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class Task8Stickers {
    private final String url = "http://localhost/litecart/";
    private final String items = "//li[contains(@class, 'product')]";
    private final String stickers = ".//*[starts-with(@class, 'sticker')]";
    private final String name = ".//*[@class='name']";

    private WebDriver driver;
    private WebDriverWait wait;

    @Before
    public void start() {
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 10);
    }

    @Test
    public void checkStickers() {
        driver.get(url);
        driver.findElement(By.name("login")).click();

        List<WebElement> items = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(this.items)));

        int expectedNumberOfStickers = 1;
        for (WebElement item : items) {
            List<WebElement> stickersOnItem = item.findElements(By.xpath(stickers));
            Assert.assertEquals(
                    item.findElement(By.xpath(name)).getAttribute("innerText") + "'s sticker count: ",
                    expectedNumberOfStickers,
                    stickersOnItem.size());
        }
    }

    @After
    public void stop() {
        driver.quit();
        driver = null;
    }
}
