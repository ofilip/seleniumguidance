import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Set;

public class Task14NewWindow {
    private WebDriver driver;
    private WebDriverWait wait;

    private final String adminUrl = "http://localhost/litecart/admin/";
    private final String countriesUrl = "http://localhost/litecart/admin/?app=countries&doc=countries";

    private final String adminLinks = "//a[contains(@href, 'admin')]";
    private final String countriesHeader = "//h1[contains(text(), 'Countries')]";
    private final String countriesHeaderItems = "//table[@class='dataTable']//tr[@class='header']/th";
    private final String countriesRows = "//table[@class='dataTable']//tr[@class='row']";
    private final String externalLinks = "//*[@class='fa fa-external-link']";


    private final String countryColumnName = "Name";

    @Before
    public void start() {
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 10);
    }

    @Test
    public void Task14NewWindow() {
        loginAsAdmin();
        openCountriesPage();
        openACountry();

        // Open each external link in the new window. Wait till each window is opened. Close the window.
        List<WebElement> linksList = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(externalLinks)));
        for (WebElement link : linksList) {
            String mainWindow = driver.getWindowHandle();
            Set<String> oldWindows = driver.getWindowHandles();
            click(link);
            String newWindow = wait.until(thereIsWindowOtherThan(oldWindows));
            driver.switchTo().window(newWindow);
            driver.close();
            driver.switchTo().window(mainWindow);
        }
    }

    public ExpectedCondition<String> thereIsWindowOtherThan(Set<String> oldWindows) {
        return handle -> {
            Set<String> handles = driver.getWindowHandles();
            handles.removeAll(oldWindows);
            return handles.size() > 0 ? handles.iterator().next() : null;
        };
    }

    private void openACountry() {
        List<WebElement> headers = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(this.countriesHeaderItems)));
        int countryNameIndex = findColumnIndex(headers, this.countryColumnName);

        if (countryNameIndex < 0) {
            String message = String.format("The country column named \"%s\" not found.", this.countryColumnName);
            Assert.assertTrue(message, false);
        }

        int countryNumber = 1;
        String firstCountryLocator = String.format("%s[%s]/td[%s]//a", this.countriesRows, countryNumber, countryNameIndex);
        WebElement country = driver.findElement(By.xpath(firstCountryLocator));
        country.click();
    }

    public void loginAsAdmin() {
        driver.get(adminUrl);
        wait.until(ExpectedConditions.elementToBeClickable(By.name("username")))
                .sendKeys("admin");
        wait.until(ExpectedConditions.elementToBeClickable(By.name("password")))
                .sendKeys("admin");
        wait.until(ExpectedConditions.elementToBeClickable(By.name("login"))).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(this.adminLinks)));
    }

    public void openCountriesPage() {
        driver.get(countriesUrl);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(this.countriesHeader)));
    }

    /**
     * Goes through the table columns at the given row, finding the index of the column with the specified text
     * @param columns list of columns in the table
     * @param columnName the name of the column
     * @return the index of the column named columnName. -1 when no match found.
     */
    private int findColumnIndex(List<WebElement> columns, String columnName) {
        int numberOfColumns = columns.size();

        for (int i = 0; i < numberOfColumns; ++i) {
            String actualName = columns.get(i).getAttribute("innerText");
            if (actualName.equals(columnName)) {
                return i + 1;
            }
        }
        return -1;
    }

    private void click(WebElement element) {
        System.out.println("Click on the element: " + element.toString());
        wait.until(ExpectedConditions.elementToBeClickable(element))
                    .click();
    }

    @After
    public void stop() {
        driver.quit();
        driver = null;
    }
}
