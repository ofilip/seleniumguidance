import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Task9_2GeoZonesSorting {
    private final String geoZonesUrl = "http://localhost/litecart/admin/?app=geo_zones&doc=geo_zones";
    private final String geoZonesHeaderItems = "//table[@class='dataTable']//tr[@class='header']/th";
    private final String geoZonesRows = "//table[@class='dataTable']//tr[@class='row']";
    private final String editZonesHeaderItems = "//table[@id='table-zones']//tr[@class='header']/th";
    private final String timeZonesRows = "//table[@id='table-zones']//tr[not(contains(@class, 'header'))]";

    private final String countryColumnName = "Name";
    private final String zoneNameColumn = "Zone";
    private final String innerText = "innerText";

    private WebDriver driver;
    private WebDriverWait wait;

    @Before
    public void start() {
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 10);
        driver.get(geoZonesUrl);

        // login
        wait.until(ExpectedConditions.elementToBeClickable(By.name("username"))).sendKeys("admin");
        wait.until(ExpectedConditions.elementToBeClickable(By.name("password"))).sendKeys("admin");
        wait.until(ExpectedConditions.elementToBeClickable(By.name("login"))).click();
    }

    @Test
    public void checkZonesSorting9_2() {
        List<WebElement> headerFields = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(this.geoZonesHeaderItems)));

        int countryNameIndex = findColumnIndex(headerFields, this.countryColumnName);
        if (countryNameIndex < 0) {
            String message = String.format("The country column named \"%s\" not found.", this.countryColumnName);
            Assert.assertTrue(message, false);
        }

        List<WebElement> rows = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(this.geoZonesRows)));
        int numberOfCountries = rows.size();

        for (int i = 1; i <= numberOfCountries; i++) {
            List<String> zones = new ArrayList<>();
            wait.until(ExpectedConditions.elementToBeClickable(
                    By.xpath(String.format("%s[%d]/td[%d]/a", this.geoZonesRows, i, countryNameIndex))))
                    .click();

            List<WebElement> editZoneHeaders = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(this.editZonesHeaderItems)));
            int zoneNameIndex = findColumnIndex(editZoneHeaders, this.zoneNameColumn);
            if (zoneNameIndex < 0) {
                String message = String.format("Zone name column named %s not found.%n", this.zoneNameColumn);
                Assert.assertTrue(message, false);
            }

            List<WebElement> zoneRows = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(this.timeZonesRows)));
            int timeZoneNumberOfRows = zoneRows.size();

            for (int j = 1; j < timeZoneNumberOfRows; j++) {
                WebElement zone = driver.findElement(By.xpath(String.format("%s[%d]/td[%d]/select/option[@selected='selected']", this.timeZonesRows, j, zoneNameIndex)));
                String zoneName = zone.getAttribute(innerText);
                zones.add(zoneName);
            }

            List<String> zonesSortedAlphabetically = new ArrayList<>(zones);
            Collections.sort(zonesSortedAlphabetically);

            Assert.assertEquals("Time zones not sorted alphabetically.", zonesSortedAlphabetically, zones);
            driver.navigate().back();
        }
    }

    /**
     * Goes through the table columns at the given row, finding the index of the column with the specified text
     *
     * @param columns    list of columns in the table
     * @param columnName the name of the column
     * @return the index of the column named columnName. -1 when no match found.
     */
    private int findColumnIndex(List<WebElement> columns, String columnName) {
        int numberOfColumns = columns.size();

        for (int i = 0; i < numberOfColumns; ++i) {
            String actualName = columns.get(i).getAttribute(innerText);
            if (actualName.equals(columnName)) {
                return i + 1;
            }
        }
        return -1;
    }

    @After
    public void stop() {
        driver.quit();
        driver = null;
    }
}
